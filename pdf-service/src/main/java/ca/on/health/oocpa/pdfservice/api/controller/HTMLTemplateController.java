/**
 * 
 */
package ca.on.health.oocpa.pdfservice.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import ca.on.health.oocpa.pdfservice.api.model.LetterTemplate;
import ca.on.health.oocpa.pdfservice.api.model.PDFResponse;
import ca.on.health.oocpa.pdfservice.api.services.template.HTMLTemplateMaintain;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author WangJi
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pdfapi")
public class HTMLTemplateController {
	  private Logger logger = LoggerFactory.getLogger(HTMLTemplateController.class);

	    @Autowired HTMLTemplateMaintain templateService;
		@PostMapping("/add")
		@ApiOperation(value = "Save OOCPA Letter Template", notes = "Save OOCPA Template Letter.", response = PDFResponse.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "OK"),
	            @ApiResponse(code = 401, message = "Unauthorized"),
	            @ApiResponse(code = 403, message = "Forbidden"),
	            @ApiResponse(code = 404, message = "Not Found"),
	            @ApiResponse(code = 500, message = "Internal Server Error")})
		 public ResponseEntity addRequest(@RequestBody LetterTemplate template) throws Exception {

	        logger.debug("Recieved 'POST' Request on /tempalte/add endpoint");
	
		   templateService.add(template);
	       Gson gson = new Gson();

		   if(logger.isInfoEnabled())
				logger.info("OOCPA Request: {}",gson.toJson(template));

			return new ResponseEntity<>(gson.toJson(template), HttpStatus.OK);
		}

		@PostMapping("/update")
		@ApiOperation(value = "Update OOCPA Letter Template", notes = "Update OOCPA Template Letter.", response = LetterTemplate.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "OK"),
	            @ApiResponse(code = 401, message = "Unauthorized"),
	            @ApiResponse(code = 403, message = "Forbidden"),
	            @ApiResponse(code = 404, message = "Not Found"),
	            @ApiResponse(code = 500, message = "Internal Server Error")})
		 public ResponseEntity updateRequest(@RequestBody LetterTemplate template) throws Exception {

	        logger.debug("Recieved 'POST' Request on /tempalte/update endpoint");
	
		   templateService.update(template);
	       Gson gson = new Gson();

		   if(logger.isInfoEnabled())
				logger.info("OOCPA Request: {}",gson.toJson(template));

			return new ResponseEntity<>(gson.toJson(template), HttpStatus.OK);
		}
		@PostMapping("/end")
		@ApiOperation(value = "End OOCPA Letter Template", notes = "End OOCPA Template Letter.", response = LetterTemplate.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "OK"),
	            @ApiResponse(code = 401, message = "Unauthorized"),
	            @ApiResponse(code = 403, message = "Forbidden"),
	            @ApiResponse(code = 404, message = "Not Found"),
	            @ApiResponse(code = 500, message = "Internal Server Error")})
		 public ResponseEntity endRequest(@RequestBody LetterTemplate template) throws Exception {

	        logger.debug("Recieved 'POST' Request on /tempalte/end endpoint");
	
		   templateService.add(template);
	       Gson gson = new Gson();

		   if(logger.isInfoEnabled())
				logger.info("OOCPA Request: {}",gson.toJson(template));

			return new ResponseEntity<>(gson.toJson(template), HttpStatus.OK);
		}

}
