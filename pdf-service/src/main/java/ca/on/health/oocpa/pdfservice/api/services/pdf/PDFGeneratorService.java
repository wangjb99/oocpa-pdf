package ca.on.health.oocpa.pdfservice.api.services.pdf;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ca.on.health.oocpa.pdfservice.api.controller.PDFGenController;
import ca.on.health.oocpa.pdfservice.api.model.LetterReq;
import ca.on.health.oocpa.pdfservice.api.model.PDFResponse;
import ca.on.health.oocpa.pdfservice.api.util.HTMLTemplateUtil;
import ca.on.health.oocpa.pdfservice.api.util.PDFUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class PDFGeneratorService {
	
	  private Logger logger = LoggerFactory.getLogger(PDFGenController.class);

	   private Configuration config;
	   private Map<String, Object> root = new HashMap<>();
	    
	public PDFResponse createPDF(LetterReq req) throws Exception {
		PDFResponse pdf = new PDFResponse();
		PDFUtil pdfUtil= new PDFUtil();
    		
		/* ------------------------------------------------------------------------ */
        /* You usually do these for MULTIPLE TIMES in the application life-cycle:   */

        /* Create a data-model */
        Map root = new HashMap();
        root.put("letterReq", req);
        
        /* Get the template (uses cache internally) */
		Template temp = config.getTemplate(req.getLetterTemplate());
		
		 /* Merge data-model with template */
        StringWriter sWriter = new StringWriter();
        try {
        	temp.process(root, sWriter);
        } catch(TemplateException ex){
        	
        }
        
        String htmlStr = sWriter.toString();

        pdf.setPdf(pdfUtil.generatePdf(htmlStr));

        String fileName = "OOCPA-Letter-" + req.getFirstName() + "-" + req.getLastName() + ".pdf";
        pdf.setFileName(fileName);
        pdf.setDate(new Date());
        
        Files.write(new File(fileName).toPath(),pdf.getPdf()); //"C:\\claim-middle-support\\Prior-Approval\\pdf\\"+ fileName
        
		return pdf;
		
	}
	
	@PostConstruct
	  public void init(){
	     // init code goes here
	    /* Create and adjust the configuration singleton */

			HTMLTemplateUtil htmlUtil = new HTMLTemplateUtil();		
			config = htmlUtil.setupConfig();

	  }
}
