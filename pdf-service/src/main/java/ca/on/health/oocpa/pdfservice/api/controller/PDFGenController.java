/**
 * 
 */
package ca.on.health.oocpa.pdfservice.api.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import ca.on.health.oocpa.pdfservice.api.model.LetterReq;
import ca.on.health.oocpa.pdfservice.api.model.PDFResponse;
import ca.on.health.oocpa.pdfservice.api.services.pdf.PDFGeneratorService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author WangJi
 *
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pdfapi")
public class PDFGenController {
	  private Logger logger = LoggerFactory.getLogger(PDFGenController.class);

	    @Autowired PDFGeneratorService pdfService;
		@PostMapping("/generate")
		@ApiOperation(value = "Save OOCPA WIP Request", notes = "Save OOCPA WIP Request.", response = PDFResponse.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "OK"),
	            @ApiResponse(code = 401, message = "Unauthorized"),
	            @ApiResponse(code = 403, message = "Forbidden"),
	            @ApiResponse(code = 404, message = "Not Found"),
	            @ApiResponse(code = 500, message = "Internal Server Error")})
		 public ResponseEntity generateRequest(@RequestBody LetterReq request) throws Exception {

	        logger.debug("Recieved 'POST' Request on /pdfapi/generate endpoint");
	
	        PDFResponse response = pdfService.createPDF(request);

	        Gson gson = new Gson();

			if(logger.isInfoEnabled())
				logger.info("OOCPA Request: {}",gson.toJson(request));

			return new ResponseEntity<>(gson.toJson(response), HttpStatus.OK);
		}
}
