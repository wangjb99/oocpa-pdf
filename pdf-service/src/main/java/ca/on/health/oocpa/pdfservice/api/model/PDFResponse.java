package ca.on.health.oocpa.pdfservice.api.model;

import java.util.Date;

public class PDFResponse {
	private byte[] pdf;
	private Date date;
	private String fileName;
	public byte[] getPdf() {
		return pdf;
	}
	public void setPdf(byte[] pdf) {
		this.pdf = pdf;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


}
