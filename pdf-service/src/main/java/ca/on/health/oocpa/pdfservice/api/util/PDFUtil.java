/**
 * 
 */
package ca.on.health.oocpa.pdfservice.api.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openhtmltopdf.extend.FSSupplier;
import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder.FontStyle;
import com.openhtmltopdf.pdfboxout.PdfBoxRenderer;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder.PdfAConformance;

import ca.on.health.oocpa.pdfservice.api.controller.PDFGenController;

/**
 * @author WangJi
 *
 */
public class PDFUtil {
	  private Logger logger = LoggerFactory.getLogger(PDFUtil.class);


    public byte[] generatePdf(final String html) throws Exception {
        logger.debug("in generate pdf");
        PdfRendererBuilder builder = new PdfRendererBuilder();
        builder.withHtmlContent(html, "/");
 
/*
 * Using usePdfAConformance resulting in missing fonts and other attributes #326
 */
//     PdfAConformance conform = PdfAConformance.PDFA_1_A;
//     builder.useFastMode();
//       builder.usePdfAConformance(conform);
        
/*        builder.useFont(getFont("fonts/NotoSans-Regular.ttf"), "noto", 400, FontStyle.NORMAL, true);

        builder.useFont(getFont("fonts/NotoSans-Bold.ttf"), "noto", 700, FontStyle.NORMAL, true);

        builder.useFont(getFont("fonts/NotoSans-BoldItalic.ttf"), "noto", 700, FontStyle.ITALIC, true);
        
        builder.useFont(getFont("fonts/NotoSans-Italic.ttf"), "noto", 400, FontStyle.ITALIC, true);
*/        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        builder.toStream(outputStream);
        try (PdfBoxRenderer pdfBoxRenderer = builder.buildPdfRenderer()) {
            pdfBoxRenderer.layout();
            pdfBoxRenderer.createPDF();
        } catch (Exception e) {
        	e.printStackTrace();
            logger.error("Error encountered while creating PDF", e);
            
        }
        outputStream.close();
        return outputStream.toByteArray();
    }
    private FSSupplier<InputStream> getFont(String fileName) {
        return new FSSupplier<InputStream>() {
            @Override
            public InputStream supply() {
                return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            }
        };
    }
}
