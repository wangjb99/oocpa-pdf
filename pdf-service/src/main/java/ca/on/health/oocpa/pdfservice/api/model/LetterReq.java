/**
 * 
 */
package ca.on.health.oocpa.pdfservice.api.model;

/**
 * @author WangJi
 *
 */
public class LetterReq {
	private String letterTemplate;
	private String version;
	private String firstName;
	private String lastName;
	public String getLetterTemplate() {
		return letterTemplate;
	}
	public void setLetterTemplate(String letterTemplate) {
		this.letterTemplate = letterTemplate;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
