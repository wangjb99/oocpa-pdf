package ca.on.health.oocpa.pdfservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {
	
	/**
	 * The primary interface into the spring framework. Selects the base package for the service
	 * controllers to display documentation for using a path selector. Currently anything past 
	 * localhost:8080/
	 */


	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select().apis(RequestHandlerSelectors.basePackage("ca.on.health.oocpa.pdfservice.api.controller"))
				.paths(PathSelectors.regex("/pdfapi/.*"))
				.build()
				.apiInfo(apiInfo());
	}
	
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("OOCPA PDF Api Documentation")
            .description("This is the OOCPA PDF Api documentation. ")
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .termsOfServiceUrl("")
            .version("1.0.0")
            .build();
    }	
	
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
    	registry.addRedirectViewController("/pdfapi/documentation/v2/api-docs", "/v2/api-docs");
    	registry.addRedirectViewController("/pdfapi/documentation/swagger-resources", "/swagger-resources");
    	registry.addRedirectViewController("/pdfapi/documentation", "/api/documentation/swagger-ui.html");
    	registry.addRedirectViewController("/pdfapi/documentation/", "/api/documentation/swagger-ui.html");
    }
    
	/**
	 * Adds the the resource locations to the swagger ui
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/pdfapi/documentation/**")
				.addResourceLocations("classpath:/META-INF/resources/");
		
		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}
