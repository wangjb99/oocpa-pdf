package ca.on.health.oocpa.pdfservice.api.services.template;

import org.springframework.stereotype.Service;

import ca.on.health.oocpa.pdfservice.api.model.LetterTemplate;

@Service
public class HTMLTemplateMaintain {

	public LetterTemplate add(LetterTemplate template) {
		return template;
	}
	public LetterTemplate update(LetterTemplate template) {
		return template;
	}
	public LetterTemplate end(String id) {
		LetterTemplate template= find(id);
		return template;
	}
	public LetterTemplate find(String id) {
		LetterTemplate template= new LetterTemplate();
		return template;
	}
}
